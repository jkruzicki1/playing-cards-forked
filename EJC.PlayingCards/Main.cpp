// Playing Cards
// Ethan Corrigan
// Forked by Jacob Kruzicki

#include <iostream>
#include <conio.h>

using namespace std;

enum class Rank
{
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace
};

enum class Suit
{
	Diamonds,
	Hearts,
	Spades,
	Clubs
};

struct Card
{
	Rank rank;
	Suit suit;
};

void PrintCard(const Card &card)
{
	switch (card.rank)
	{
	case Rank::Two:
		cout << "Two";
		break;
	case Rank::Three:
		cout << "Three";
		break;
	case Rank::Four:
		cout << "Four";
		break;
	case Rank::Five:
		cout << "Five";
		break;
	case Rank::Six:
		cout << "Six";
		break;
	case Rank::Seven:
		cout << "Seven";
		break;
	case Rank::Eight:
		cout << "Eight";
		break;
	case Rank::Nine:
		cout << "Nine";
		break;
	case Rank::Ten:
		cout << "Ten";
		break;
	case Rank::Jack:
		cout << "Jack";
		break;
	case Rank::Queen:
		cout << "Queen";
		break;
	case Rank::King:
		cout << "King";
		break;
	case Rank::Ace:
		cout << "Ace";
		break;
	default:
		cout << "Invalid";
		break;
	}

	cout << " of ";

	switch (card.suit)
	{
	case Suit::Clubs:
		cout << "Clubs";
		break;
	case Suit::Diamonds:
		cout << "Diamonds";
		break;
	case Suit::Hearts:
		cout << "Hearts";
		break;
	case Suit::Spades:
		cout << "Spades";
		break;
	default:
		cout << "Invalid";
		break;
	}

	cout << endl;
}

Card HighCard(const Card& card1, const Card& card2)
{
	if (card1.rank > card2.rank)
	{
		return card1;
	}
	else if (card1.rank < card2.rank)
	{
		return card2;
	}
}

int main()
{
	Card c1;
	c1.rank = Rank::Ten;

	Card c2;
	c2.rank = Rank::Ace;



	(void)_getch();
	return 0;
}
